SHELL=/bin/bash
projects=projects

build-%:
	rm -rf content/ && rm -rf public/ && rm -rf resources/ && cp -r $(projects)/$* content/ && hugo


serve:
	hugo serve